// Import dependencies
import path from "path";
import fs from 'fs';
import express from 'express';
import bodyParser from 'body-parser';
import Database from 'better-sqlite3';
const { config, engine } = require('express-edge');
// Import router
import router from './router';

// Initialize the express engine
const app: express.Application = express();

// Take a port 3000 for running server.
const port: number = 5000;

export const db = new Database(path.join(__dirname, '/database/data.db'), { verbose: () => { } });

/**
 * creation de la base de données, et insertion des données
 * A ne lancer qu'une seule fois
 */
/*
 
 */

//    const migration = fs.readFileSync(path.join(__dirname, '/database/DB_table.sql'), 'utf8');
//   db.exec(migration);
     

//  const migrationData = fs.readFileSync(path.join(__dirname, '/database/data.sql'), 'utf8');
//   db.exec(migrationData);

/*
{
 const migrationData = fs.readFileSync(path.join(__dirname, '/database/data.sql'), 'utf8');
 db.exec(migrationData);
}
*/
// define the templating engine
app.use(engine);
//const db = app.locals.db
app.locals.db = db;
// define the Views folder
app.set("views", path.join(__dirname, "./views"));

app.use(
    express.static(path.join(__dirname, "public"), { maxAge: 31557600000 })
);
// les parties pour express session : 
const sessions = require('express-session');
// creating 24 hours from milliseconds
const oneDay = 1000 * 60 * 60 * 24;

//session middleware
app.use(sessions({
    secret: "thisismysecrctekeyfhrgfgrfrty84fwir767",
    saveUninitialized:true,
    cookie: { maxAge: oneDay },
    resave: false
}));
// parsing the incoming data
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//serving public file
app.use(express.static(__dirname));

var session;

const cookieParser = require("cookie-parser");

app.use(cookieParser());

// to read request form body
app.use(bodyParser.urlencoded({ extended: false }));

// Routes
router(app);

// Server setup
app.listen(port, () =>
{
    console.log(`TypeScript with Express http://localhost:${port}/`);
});