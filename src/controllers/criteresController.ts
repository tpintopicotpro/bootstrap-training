import { Request, Response } from "express-serve-static-core";
import Criteres from "../model/CriteresModel";
import User from "../model/UserModel";
import HomeController from "./HomeController";

export default class CriteresController {

    static cocheUnCritere(req: Request, res: Response): void {
        const result = req.body;

        
        //@ts-ignore
        const criteres = Criteres.getCriteresComp(result.compId)
        //const criteres = db.prepare('SELECT * FROM criteres WHERE competences_idcompetences = ?').all(result.compId);
        //@ts-ignore
        let recup = User.getCriteres()
        // const recup = db.prepare('SELECT * FROM user_has_criteres').all();

        const criteresAlreadyChecked: number[] = []
        recup.forEach((element: any) => criteresAlreadyChecked.push(element.criteres_idcriteres));
        //console.log(criteresAlreadyChecked)
        //@ts-ignore
        let myUser = req.session.user
        for (let critere of criteres) {
            const critId = critere.idcriteres;

            let checkboxId: number = req.body['checkbox' + critId];
            //console.log (req.body['checkbox' + critId])
            if (checkboxId !== undefined)
            {
                if (!criteresAlreadyChecked.includes(+checkboxId)) {
                    User.addUserCriteres(myUser, critId)
                    //db.prepare("INSERT INTO user_has_criteres (user_idUser, criteres_idcriteres) VALUES (?, ?)").run(myUser, critId);
                }
                else
                {
                    console.log('critere est deja coché')
                }
            }
            else if (criteresAlreadyChecked.includes(critId))
            {
                User.delUserCriteres(myUser, critId)
                // db.prepare("DELETE FROM user_has_criteres WHERE user_idUser = ? AND criteres_idcriteres = ? ").run(myUser, critId)
            }
        }
 
        // for (let element of criteres){
        //     console.log (element.idcriteres)
        //     if(criteresAlreadyChecked.includes(element.idcriteres)){
        //         element.checked ='checked'
        //     }
        //     else{
        //         element.checked = ''
        //     }
        // }
        HomeController.index(req, res);
    }
}