import { Request, Response } from "express-serve-static-core";
import session from "express-session";
import { appendFile } from "fs";
import { createIndexSignature } from "typescript";
import CompetencesModel from "../model/CompetencesModel";
import User from "../model/UserModel";
const bcrypt = require ('bcryptjs');

export default class HomeController

{

    static index(req: Request, res: Response): void
    {
        
        let stmt =  CompetencesModel.getCompetence();
        let stmt2 = CompetencesModel.getIndexCompetence2();

        res.render('pages/index', {
            competence : stmt ,
            competence2: stmt2 ,
        });
    }

    static about(req: Request, res: Response): void
    {
        res.render('pages/about', {
            title: 'About',
        });
    }
    static showRegister (req: Request, res: Response): void { 

        res.render('pages/register', {
           

        })

    }

    static register(req: Request, res: Response): void { 
        const db = req.app.locals.db;  
        const hash = bcrypt.hashSync(req.body.password)
        const stmt = User.getByUsername(req.body.username)
        //const stmt = db.prepare('INSERT INTO user ("username","password","email") VALUES (?,?,?)').run(req.body.username, hash, req.body.email)
        // let arrUser = stmt.map((el : any) => el.username);
        // console.log (arrUser)
        
        if (stmt){
            res.render ('pages/register', {

            })
        }
        else {
            User.register(req.body.username, hash, req.body.email); 
            res.render('pages/login', {
           
        })
        }
        
       
       
    }
    static showLogin(req: Request, res: Response): void {
        res.render('pages/login')

    }

    static login(req: Request, res: Response): void {
        let Username = req.body.username
        let stmt = User.getUsername(Username)
       // const stmt = db.prepare('SELECT * FROM user WHERE username=?').get(req.body.username);
         //console.log(stmt)
        // console.log('Le cookie : ',req.session.cookie)
        
       
        if(stmt==undefined){
            res.redirect('/login')
        }
        else if(stmt.username == req.body.username){
        //@ts-ignore
        req.session.user= stmt.idUser
        //@ts-ignore
        let myUser = req.session.user
        let comp=bcrypt.compareSync(req.body.password, stmt.password)
        // let comp2 = bcrypt.compareSync()

            if(comp){
                //session=req.session;
               // session.userid=req.body.username;
                //console.log("login validé")
                User.getUserCrit(myUser)
                //const data = db.prepare('SELECT * FROM user_has_criteres WHERE user_idUser = ?').run(myUser);
                HomeController.index(req, res)
            }
            else{
                //console.log("tu t'es trompé")
                HomeController.checkLogin (req, res);
            }
        }}


    static checkLogin (req: Request, res: Response): void {

    }
        
    static logout (req: Request, res: Response): void {
        if (req.session) {
            req.session.destroy(err => {
              if (err) {
                res.status(400).send('Unable to log out')
              } else {
                res.redirect('/')
              }
            });
          } else {
            res.end()
          }
    }
};