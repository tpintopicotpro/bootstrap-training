import { Request, Response } from "express-serve-static-core";
import CompetencesModel from "../model/CompetencesModel";
import Criteres from "../model/CriteresModel";
import User from "../model/UserModel";
//import { Tcompetences } from "../type";

export default class CompetencesController {
    static competences(req: Request, res: Response): void {
        const db = req.app.locals.db;
        const criteresAlreadyChecked: number[] = []
        const id = +req.params.id;
        //@ts-ignore
        let myUser = req.session.user
        //console.log (myUser)
        // const comp mettre (id)
        // const crit mettre (id)

        // const pivot mettre (myUser)
        const competences = CompetencesModel.getComp(id)
        //const competences = db.prepare('SELECT * FROM competences WHERE idcompetences = ?').all(id);
        const criteres = Criteres.getCriteresComp(id)
        //const criteres = db.prepare('SELECT * FROM criteres WHERE competences_idcompetences = ?').all(id);
        const data = User.getUserCrit(myUser)
        const recup = User.getUserCrit(myUser)

        let arrCrit = data.map((el : any) => el.criteres_idcriteres);
        //data.forEach((element: any) => arrCrit.push(element.criteres_idcriteres));
        
        
        recup.forEach((element: any) => criteresAlreadyChecked.push(element.criteres_idcriteres));
        
        criteres.forEach((item: any) => {
            if(criteresAlreadyChecked.includes(item.idcriteres)){
                item.checked ='checked'
            }
            else{
                item.checked = ''
            }
        })
        
        res.render('pages/competences', { 
            title: 'Competences',
            competences: competences,
            criteres: criteres,
            checkbox: data,
            //checkbox : recup, 
        })
    }
}

/**
 * Faire une fonction qui permet d'afficher toutes les competences et leurs criteres une a une
 * @param req
 * @param res
 */
//     static programme (req: Request, res: Response): void {
//         const db = req.app.locals.db;
//         const stmt = db.prepare('SELECT * FROM competences').all()
//         const stmt2 = db.prepare('SELECT * FROM criteres').all()
//         //stmt.map (Elm => elm.checked = 'checked')
//         res.render('pages/programme', {
//             title: 'Competences',
//             competences: stmt ,
//             criteres: stmt2 ,
//         });
//     }
// }
