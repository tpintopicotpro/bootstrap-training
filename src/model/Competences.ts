export default class Competences {

    idcompetences : string;

    title : string;

    content : string;

    constructor(idcompetences : string, title : string, content : string) {
        this.idcompetences = idcompetences;
        this.title = title;
        this.content = content;
    }

    getTitle() : string {
        return this.title;
    }
}