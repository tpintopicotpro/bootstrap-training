import { Database } from "better-sqlite3";
import { Request, Response } from "express-serve-static-core";
import { db } from "../server";
import Competences from "./Competences";

export default class CompetencesModel {
    
    static getCompetence(): any {
       return db.prepare('SELECT * FROM competences WHERE idcompetences < 5').all();
    }
        
    static getIndexCompetence2(): any {
       return db.prepare('SELECT * FROM competences WHERE idcompetences > 4').all();
    }

    static getComp(id:Number):any {
       return  db.prepare('SELECT * FROM competences WHERE idcompetences = ?').all(id);
    }

}