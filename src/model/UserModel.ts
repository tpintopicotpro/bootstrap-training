import { db } from "../server";

export default class User {
    static getID(): void {

    }

    static register(username:string, hash:string, Email: string): any {
        return db.prepare('INSERT INTO user ("username","password","email") VALUES (?,?,?)').run(username, hash, Email)
    }

    static getUsername(username:Text): any {
        return  db.prepare('SELECT * FROM user WHERE username=?').get(username);
    }

    static getByUsername (username: string): any {
        return  db.prepare('SELECT * FROM user WHERE username=?').get(username);
    }

    static getUserCrit (userId:Number): any {
        return db.prepare('SELECT * FROM user_has_criteres WHERE user_idUser = ?').run(userId)
    }


    static getCriteres (): any {
        return db.prepare('SELECT * FROM user_has_criteres').all();
    }
    static addUserCriteres (id:Number, critId:number): any {
       
        return db.prepare("INSERT INTO user_has_criteres (user_idUser, criteres_idcriteres) VALUES (?, ?)").run(id, critId);

    }
    static delUserCriteres (id:Number, critId:number): any {
        
        return db.prepare("DELETE FROM user_has_criteres WHERE user_idUser = ? AND criteres_idcriteres = ? ").run(id, critId)

    }
}