import { Request, Response } from "express-serve-static-core";
import HomeController from "../controllers/HomeController";
export default class AuthListener {

static auth (req: any, res: any, next: any){

        if (req.session.user) next();
        else {
                res.redirect('/login')
        };
}
}

// module.exports.isAuthorized  = function(req: { session: { userId: any; }; }, res: any, next: (arg0: Error | undefined) => any) {

//     userId.findById(req.session.userId).exec(function (error: any, user: null) {
//         if (error) {
//             return next(error);
//         } else {      
//             if (user === null) {     
//                 var err = new Error('Not authorized! Go back!');
//                 err.status = 401;
//                 return next(err);
//             } else {
//                 return next();
//             }
//         }
//     });
// }
