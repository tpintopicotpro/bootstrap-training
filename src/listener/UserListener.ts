import { checkPrimeSync } from "crypto";
import { body, validationResult } from "express-validator";

export default class UserListener {

    static register (req: any, res: any): void {
       
        body('username').isEmail(),
        // password must be at least 5 chars long
        body('password').isLength({ min: 5 }),
        (req: any, res: any) => {
            // Finds the validation errors in this request and wraps them in an object with handy functions
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
              return res.render("/login");
            }
            else{
            }

    }
}}
