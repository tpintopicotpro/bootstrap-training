import { Application } from "express";
import HomeController from "./controllers/HomeController";
import CompetencesController from "./controllers/CompetenceController";
import CriteresController from "./controllers/criteresController";
import AuthListener from "./listener/AuthListener";
import UserListener from "./listener/UserListener";
import { body, validationResult } from "express-validator";

export default function route(app: Application) {
    /** Static pages **/
    app.get('/', (req, res) => {
        HomeController.index(req, res);
    });

    app.get('/about', (req, res) => {
        HomeController.about(req, res);
    });
    let auth = require('./listener/AuthListener');

    // auth(req, res, next){
    //     //if userconnecté return next else renvoie login 
    // }
    // affiche le form
    app.get('/competences/:id', AuthListener.auth ,(req, res) => {
        // if (req.params.id === '') next('pages/login');
        // else next();
        CompetencesController.competences(req, res);
    });

    // recup le form
    app.post('/criteres', (req, res) => {
        CriteresController.cocheUnCritere(req, res)
    });

    app.get('/login', (req, res) => {
        HomeController.showLogin(req, res);
        //const session=req.session;
    });

    app.post('/login', (req, res) => {
        HomeController.login(req, res);
    });
    app.get('/logout', (req, res) => {
        HomeController.logout(req, res);
    });

    app.get('/register', (req, res) => {
        HomeController.showRegister(req, res);
    });

    // app.post('/register', UserListener.register, (req, res) => {

    //     HomeController.register(req, res);
    // });

app.post('/register',
    body('username').isLength({ min: 5 }),
    body('password').isLength({ min: 5 })
    .withMessage('must be at least 5 chars long'),

    body('email').isEmail(),
    (req, res) => {
      // Finds the validation errors in this request and wraps them in an object with handy functions
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.render('pages/register')
      }
          //@ts-ignore
          HomeController.register (req, res);
    } 
  );

    app.get('/', (req, res) => {
        HomeController.showRegister(req, res);
    });
}